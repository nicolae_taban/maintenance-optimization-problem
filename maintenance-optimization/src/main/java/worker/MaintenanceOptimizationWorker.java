package worker;

import auxiliary.SimulationConfiguration;
import crossover.MaintenanceProblemCrossover;
import mutation.MaintenanceProblemMutation;
import org.uma.jmetal.experimental.componentbasedalgorithm.algorithm.multiobjective.nsgaii.NSGAII;
import org.uma.jmetal.experimental.componentbasedalgorithm.catalogue.evaluation.impl.MultithreadedEvaluation;
import org.uma.jmetal.experimental.componentbasedalgorithm.catalogue.termination.Termination;
import org.uma.jmetal.experimental.componentbasedalgorithm.catalogue.termination.impl.TerminationByEvaluations;
import org.uma.jmetal.operator.crossover.CrossoverOperator;
import org.uma.jmetal.operator.crossover.impl.IntegerSBXCrossover;
import org.uma.jmetal.operator.mutation.MutationOperator;
import org.uma.jmetal.operator.mutation.impl.IntegerPolynomialMutation;
import org.uma.jmetal.operator.mutation.impl.PolynomialMutation;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.doublesolution.DoubleSolution;
import org.uma.jmetal.solution.integersolution.IntegerSolution;
import org.uma.jmetal.util.JMetalLogger;
import org.uma.jmetal.util.bounds.Bounds;
import org.uma.jmetal.util.fileoutput.SolutionListOutput;
import org.uma.jmetal.util.fileoutput.impl.DefaultFileOutputContext;
import org.uma.jmetal.util.observer.impl.EvaluationObserver;
import org.uma.jmetal.util.observer.impl.RunTimeChartObserver;
import problem.MaintenanceOptimizationProblem;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class MaintenanceOptimizationWorker extends SwingWorker<List<Object>, List<Object>> {

    SimulationConfiguration simulationConfiguration;

    public MaintenanceOptimizationWorker(SimulationConfiguration cfg) {
        this.simulationConfiguration = cfg;
    }

    @Override
    protected List<Object> doInBackground() throws Exception {
        Problem<IntegerSolution> problem;
        NSGAII<IntegerSolution> algorithm;
        CrossoverOperator<IntegerSolution> crossover;
        MutationOperator<IntegerSolution> mutation;
        int numberOfVariables = simulationConfiguration.getNumberOFRepairsPerChromosome() * 2;

        List<Bounds<Integer>> integerBounds = new ArrayList<>(numberOfVariables);

        for (int i = 0; i < numberOfVariables; i++) {
            if (i % 2 == 0) {
                integerBounds.add(Bounds.create(0, simulationConfiguration.getNumberOfSystems() - 1));
            } else {
                integerBounds.add(Bounds.create(0, simulationConfiguration.getNumberOfComponents() - 1));
            }
        }

        double crossoverProbability = simulationConfiguration.getCrossoverProbability();
        if (crossoverProbability != 0) {
            crossoverProbability = crossoverProbability / 100;
        } else {
            crossoverProbability = 0;
        }

        double mutationProbability = simulationConfiguration.getMutationProbability();
        if (mutationProbability != 0) {
            mutationProbability = mutationProbability / 100;
        } else {
            mutationProbability = 0;
        }
        crossover = new MaintenanceProblemCrossover(
                crossoverProbability,
                1,
                simulationConfiguration.getNumberOfSystems(),
                simulationConfiguration.getNumberOfComponents(),
                simulationConfiguration.getNumberOFRepairsPerChromosome()
        );
        mutation = new MaintenanceProblemMutation(
                mutationProbability,
                simulationConfiguration.getNumberOfSystems(),
                simulationConfiguration.getNumberOfComponents(),
                simulationConfiguration.getNumberOFRepairsPerChromosome()
        );

        Termination termination = new TerminationByEvaluations(simulationConfiguration.getNumberOfChromosomes() * simulationConfiguration.getNumberOfGenerations());

        problem = new MaintenanceOptimizationProblem(
                integerBounds,
                numberOfVariables,
                simulationConfiguration.getInitialComponentState(),
                simulationConfiguration.getPerComponentCost(),
                simulationConfiguration.getNumberOfSystems(),
                simulationConfiguration.getNumberOfComponents(),
                simulationConfiguration.getNumberOFRepairsPerChromosome()
                );

        algorithm =
                (NSGAII<IntegerSolution>) new NSGAII<>(
                        problem, simulationConfiguration.getNumberOfChromosomes(), simulationConfiguration.getNumberOfChromosomes(), crossover, mutation, termination)
                .withEvaluation(new MultithreadedEvaluation<>(4, problem));

        EvaluationObserver evaluationObserver = new EvaluationObserver(100);
        RunTimeChartObserver<DoubleSolution> runTimeChartObserver =
                new RunTimeChartObserver<>("NSGA-II", 100, null);

        algorithm.getObservable().register(evaluationObserver);
        algorithm.getObservable().register(runTimeChartObserver);

        algorithm.run();

        new SolutionListOutput(algorithm.getResult())
                .setVarFileOutputContext(new DefaultFileOutputContext("VAR.csv", ","))
                .setFunFileOutputContext(new DefaultFileOutputContext("FUN.csv", ","))
                .print();

        JMetalLogger.logger.info("Objectives values have been written to file FUN.csv");
        JMetalLogger.logger.info("Variables values have been written to file VAR.csv");

        return null;
    }
}
