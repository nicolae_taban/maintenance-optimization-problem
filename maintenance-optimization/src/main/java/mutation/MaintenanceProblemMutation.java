package mutation;

import org.uma.jmetal.operator.mutation.MutationOperator;
import org.uma.jmetal.solution.integersolution.IntegerSolution;
import org.uma.jmetal.util.bounds.Bounds;
import org.uma.jmetal.util.errorchecking.JMetalException;
import org.uma.jmetal.util.pseudorandom.JMetalRandom;

import java.util.ArrayList;
import java.util.List;

public class MaintenanceProblemMutation implements MutationOperator<IntegerSolution> {
    private double mutationProbability;
    private JMetalRandom randomNumberGenerator;
    private final int numberOfSystems;
    private final int numberOfComponents;
    private final int numberOfRepairs;

    public MaintenanceProblemMutation(double probability, int numberOfSystems, int numberOfComponents, int numberOfRepairs) {
        if (probability < 0) {
            throw new JMetalException("Mutation probability is negative: " + mutationProbability);
        }
        this.mutationProbability = probability;
        this.randomNumberGenerator = JMetalRandom.getInstance();
        this.numberOfRepairs = numberOfRepairs;
        this.numberOfSystems = numberOfSystems;
        this.numberOfComponents = numberOfComponents;
    }

    @Override
    public double getMutationProbability() {
        return mutationProbability;
    }

    public void setMutationProbability(double mutationProbability) {
        this.mutationProbability = mutationProbability;
    }

    @Override
    public IntegerSolution execute(IntegerSolution solution) {
        if (null == solution) {
            throw new JMetalException("Null parameter");
        }

        doMutation(mutationProbability, solution);

        return solution;
    }

    private void doMutation(double probability, IntegerSolution solution) {
        for (int i = 0; i < solution.getNumberOfVariables(); i++) {
            if (randomNumberGenerator.nextDouble() <= probability) {
                Bounds<Integer> bounds = solution.getBounds(i);
                Integer lowerBound = bounds.getLowerBound();
                Integer upperBound = bounds.getUpperBound();
                Double randomValue = randomNumberGenerator.nextDouble();
                Integer value = lowerBound + (int)((upperBound - lowerBound) * randomValue);
                solution.setVariable(i, value);
            }
        }
        makeOffspringUnique(solution);
    }

    private void makeOffspringUnique(IntegerSolution girl) {
        List<ArrayList<Integer>> usedCombinations = new ArrayList<>();
        for (int i = 0; i < numberOfSystems; i++) {
            usedCombinations.add(i, new ArrayList<>());
        }

        for (int i = 0; i < numberOfRepairs * 2; i+=2) {
            ArrayList<Integer> theList = usedCombinations.get(girl.getVariable(i));
            if (!theList.contains(girl.getVariable(i+1))) {
                theList.add(girl.getVariable(i+1));
                usedCombinations.set(girl.getVariable(i), theList);
            } else {
                int randomSystem = randomNumberGenerator.nextInt(0, numberOfSystems - 1);
                int randomComponent = randomNumberGenerator.nextInt(0, numberOfComponents - 1);
                while (usedCombinations.get(randomSystem).contains(randomComponent)) {
                    randomSystem = randomNumberGenerator.nextInt(0, numberOfSystems - 1);
                    randomComponent = randomNumberGenerator.nextInt(0, numberOfComponents - 1);
                }
                usedCombinations.get(randomSystem).add(randomComponent);
                girl.setVariable(i, randomSystem);
                girl.setVariable(i+1, randomComponent);
            }
        }
    }
}
