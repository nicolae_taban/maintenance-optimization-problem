package auxiliary;

public class SimulationConfiguration {
    Integer maximumReplacementCost;
    Integer maximumWearPerGeneration;
    Integer numberOfSystems;
    Integer numberOfComponents;
    Integer crossoverProbability;
    Integer mutationProbability;
    Integer numberOfGenerations;
    Integer numberOfChromosomes;
    Integer numberOFRepairsPerChromosome;

    public Integer getNumberOFRepairsPerChromosome() {
        return numberOFRepairsPerChromosome;
    }

    public void setNumberOFRepairsPerChromosome(Integer numberOFRepairsPerChromosome) {
        this.numberOFRepairsPerChromosome = numberOFRepairsPerChromosome;
    }

    int[][] initialComponentState;
    int[][] perGenerationWear;
    int[][] perComponentCost;

    public Integer getMaximumReplacementCost() {
        return maximumReplacementCost;
    }

    public void setMaximumReplacementCost(Integer maximumReplacementCost) {
        this.maximumReplacementCost = maximumReplacementCost;
    }

    public Integer getMaximumWearPerGeneration() {
        return maximumWearPerGeneration;
    }

    public void setMaximumWearPerGeneration(Integer maximumWearPerGeneration) {
        this.maximumWearPerGeneration = maximumWearPerGeneration;
    }

    public Integer getNumberOfSystems() {
        return numberOfSystems;
    }

    public void setNumberOfSystems(Integer numberOfSystems) {
        this.numberOfSystems = numberOfSystems;
    }

    public Integer getNumberOfComponents() {
        return numberOfComponents;
    }

    public void setNumberOfComponents(Integer numberOfComponents) {
        this.numberOfComponents = numberOfComponents;
    }

    public Integer getCrossoverProbability() {
        return crossoverProbability;
    }

    public void setCrossoverProbability(Integer crossoverProbability) {
        this.crossoverProbability = crossoverProbability;
    }

    public Integer getMutationProbability() {
        return mutationProbability;
    }

    public void setMutationProbability(Integer mutationProbability) {
        this.mutationProbability = mutationProbability;
    }

    public Integer getNumberOfGenerations() {
        return numberOfGenerations;
    }

    public void setNumberOfGenerations(Integer numberOfGenerations) {
        this.numberOfGenerations = numberOfGenerations;
    }

    public Integer getNumberOfChromosomes() {
        return numberOfChromosomes;
    }

    public void setNumberOfChromosomes(Integer numberOfChromosomes) {
        this.numberOfChromosomes = numberOfChromosomes;
    }

    public int[][] getInitialComponentState() {
        return initialComponentState;
    }

    public void setInitialComponentState(int[][] initialComponentState) {
        this.initialComponentState = initialComponentState;
    }

    public int[][] getPerGenerationWear() {
        return perGenerationWear;
    }

    public void setPerGenerationWear(int[][] perGenerationWear) {
        this.perGenerationWear = perGenerationWear;
    }

    public int[][] getPerComponentCost() {
        return perComponentCost;
    }

    public void setPerComponentCost(int[][] perComponentCost) {
        this.perComponentCost = perComponentCost;
    }
}
