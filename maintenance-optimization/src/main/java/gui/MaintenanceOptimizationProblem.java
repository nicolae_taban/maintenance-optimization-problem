package gui;

import auxiliary.SimulationConfiguration;
import worker.MaintenanceOptimizationWorker;

import javax.swing.*;
import java.util.Random;

public class MaintenanceOptimizationProblem {
    private JPanel rootPanel;
    private JLabel numberOfChromosomesLabel;
    private JTextField numberOfChromosomesTextField;
    private JLabel numberOfGenerationsLabel;
    private JTextField numberOfGenerationsTextField;
    private JLabel numberOfSystemsLabel;
    private JTextField numberOfSystemsTextField;
    private JTextField numberOfComponentsTextField;
    private JLabel crossoverProbabilityLabel;
    private JTextField crossoverProbabilityTextField;
    private JLabel mutationProbabilityLabel;
    private JTextField mutationProbabilityTextField;
    private JLabel maximumReplacementCostLabel;
    private JLabel maximumWearPerGenerationLabel;
    private JTextField maximumReplacementCostTextField;
    private JTextField maximumWearPerGenerationTextField;
    private JButton generateInitialStateButton;
    private JTextField numberOfRepairedComponentsPerChromosomeTextField;
    private JLabel numberOfRepairedComponentsPerChromosomeLabel;

    Random random;
    SimulationConfiguration configuration;
    MaintenanceOptimizationWorker worker;

    public MaintenanceOptimizationProblem() {
        random = new Random();

        generateInitialStateButton.addActionListener(e -> {

            Integer maximumReplacementCost = tryParseInt(maximumReplacementCostTextField.getText());
            String invalidReplacementCost = "Invalid replacement cost!";
            String outOfBoundariesErrorText = "Replacement cost not in the interval 1 - 100!";
            if (null == parseParameter(maximumReplacementCost, 1, 100, invalidReplacementCost, outOfBoundariesErrorText)) {
                return;
            }

            Integer maximumWearPerGeneration = tryParseInt(maximumWearPerGenerationTextField.getText());
            String invalidMaximumWearPerGeneration = "Invalid maximum wear per generation!";
            outOfBoundariesErrorText = "Maximum wear per generation not in the interval 1 - 70!";
            if (null == parseParameter(maximumWearPerGeneration, 1, 70, invalidMaximumWearPerGeneration, outOfBoundariesErrorText)) {
                return;
            }

            Integer numberOfSystems = tryParseInt(numberOfSystemsTextField.getText());
            String invalidNumberOfSystems = "Invalid number of systems!";
            outOfBoundariesErrorText = "Number of systems not in the 1 - 25 interval!";
            if (null == parseParameter(numberOfSystems, 1, 25, invalidNumberOfSystems, outOfBoundariesErrorText)) {
                return;
            }

            Integer numberOfComponents = tryParseInt(numberOfComponentsTextField.getText());
            String invalidNumberOfComponents = "Invalid number of components!";
            outOfBoundariesErrorText = "Number of components not in the 10 - 25 interval!";
            if (null == parseParameter(numberOfComponents, 10, 25, invalidNumberOfComponents, outOfBoundariesErrorText)) {
                return;
            }

            Integer crossoverProbability = tryParseInt(crossoverProbabilityTextField.getText());
            String invalidCrossoverProbability = "Invalid crossover probability!";
            outOfBoundariesErrorText = "Crossover probability must be in the 0 - 100 interval!";
            if (null == parseParameter(crossoverProbability, 0, 100, invalidCrossoverProbability, outOfBoundariesErrorText)) {
                return;
            }

            Integer mutationProbability = tryParseInt(mutationProbabilityTextField.getText());
            String invalidMutationProbability = "Invalid mutation probability!";
            outOfBoundariesErrorText = "Mutation probability must be in the 0 - 100 interval!";
            if (null == parseParameter(mutationProbability, 0, 100, invalidMutationProbability, outOfBoundariesErrorText)) {
                return;
            }

            Integer numberOfGenerations = tryParseInt(numberOfGenerationsTextField.getText());
            String invalidNumberOfGenerations = "Invalid number of generations!";
            outOfBoundariesErrorText = "The number of generations must be strictly positive!";
            if (null == parseParameter(numberOfGenerations, 1, Integer.MAX_VALUE, invalidNumberOfGenerations, outOfBoundariesErrorText)) {
                return;
            }

            Integer numberOfChromosomes = tryParseInt(numberOfChromosomesTextField.getText());
            String invalidNumberOfChromosomes = "Invalid number of chromosomes!";
            outOfBoundariesErrorText = "There must be at least two chromosomes!";
            if (null == parseParameter(numberOfChromosomes, 2, Integer.MAX_VALUE, invalidNumberOfChromosomes, outOfBoundariesErrorText)) {
                return;
            }

            Integer numberOfRepairsPerChromosome = tryParseInt(numberOfRepairedComponentsPerChromosomeTextField.getText());
            String invalidNumberOfRepairs = "The number of repairs per chromosome is invalid!";
            outOfBoundariesErrorText = "There must be at least 6 repairs per chromosome!";
            if (null == parseParameter(numberOfRepairsPerChromosome, 6, Integer.MAX_VALUE, invalidNumberOfRepairs, outOfBoundariesErrorText)) {
                return;
            }

            int[][] initialComponentState = new int [numberOfSystems][numberOfComponents];
            int[][] perGenerationWear = new int [numberOfSystems][numberOfComponents];
            int[][] perComponentCost = new int [numberOfSystems][numberOfComponents];

            for (int i = 0; i < numberOfSystems; i++) {
                for (int j = 0; j < numberOfComponents; j++) {
                    initialComponentState[i][j] = random.nextInt(3);
                    perGenerationWear[i][j] = 10 + random.nextInt(maximumWearPerGeneration);
                    perComponentCost[i][j] = 10 + random.nextInt(50);
                }
            }

            configuration = new SimulationConfiguration();
            configuration.setMaximumReplacementCost(maximumReplacementCost);
            configuration.setMaximumWearPerGeneration(maximumWearPerGeneration);
            configuration.setNumberOfSystems(numberOfSystems);
            configuration.setNumberOfComponents(numberOfComponents);
            configuration.setCrossoverProbability(crossoverProbability);
            configuration.setMutationProbability(mutationProbability);
            configuration.setNumberOfGenerations(numberOfGenerations);
            configuration.setNumberOfChromosomes(numberOfChromosomes);
            configuration.setInitialComponentState(initialComponentState);
            configuration.setPerGenerationWear(perGenerationWear);
            configuration.setPerComponentCost(perComponentCost);
            configuration.setNumberOFRepairsPerChromosome(numberOfRepairsPerChromosome);

            worker = new MaintenanceOptimizationWorker(configuration);
            worker.execute();
        });
    }

    private Integer parseParameter(Integer parameter, int lowerBoundary, int upperBoundary, String notANumberErrorText, String outOfBoundariesErrorText) {
        if (parameter == null) {
            displayError(notANumberErrorText);
            return null;
        }
        if (!checkBoundaries(parameter, lowerBoundary, upperBoundary)) {
            displayError(outOfBoundariesErrorText);
            return null;
        }
        return parameter;
    }

    private boolean checkBoundaries(Integer number, int lower, int upper) {
     if (number < lower || number > upper) {
         return false;
     }
     return true;
    }


    Integer tryParseInt(String value) {
        try {
            Integer result;
            result = Integer.parseInt(value);
            return result;
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public void displayError(String message) {
        JOptionPane.showMessageDialog(new JFrame(), message, "Error",
                JOptionPane.ERROR_MESSAGE);
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Maintenance Optimization Problem");
        frame.setContentPane(new MaintenanceOptimizationProblem().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
