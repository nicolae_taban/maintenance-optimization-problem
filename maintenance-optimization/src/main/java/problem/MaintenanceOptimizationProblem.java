package problem;

import org.uma.jmetal.problem.AbstractGenericProblem;
import org.uma.jmetal.solution.integersolution.IntegerSolution;
import org.uma.jmetal.solution.integersolution.impl.DefaultIntegerSolution;
import org.uma.jmetal.util.bounds.Bounds;

import java.util.Arrays;
import java.util.List;

public class MaintenanceOptimizationProblem extends AbstractGenericProblem<IntegerSolution> {

    final private List<Bounds<Integer>> integerBounds;
    int[][] initialComponentState;
    int[][] perComponentCost;
    int numberOfSystems;
    int numberOfComponents;
    int numberOfRepairsPerChromosome;

    public MaintenanceOptimizationProblem(
            List<Bounds<Integer>> integerBounds,
            int numberOfVariables,
            int[][] initialComponentState,
            int[][] perComponentCost,
            int numberOfSystems,
            int numberOfComponents,
            int numberOfRepairsPerChromosome
    )
    {
        setNumberOfObjectives(2);
        setNumberOfVariables(numberOfVariables);
        setNumberOfConstraints(0);
        setName("Maintenance Optimization Problem");
        this.integerBounds = integerBounds;
        this.initialComponentState = initialComponentState;
        this.perComponentCost = perComponentCost;
        this.numberOfSystems = numberOfSystems;
        this.numberOfComponents = numberOfComponents;
        this.numberOfRepairsPerChromosome = numberOfRepairsPerChromosome;
    }

    @Override
    public IntegerSolution evaluate(IntegerSolution solution) {
        double availableSystems = 0;
        double cost = 0;

        int[][] copy = Arrays.stream(initialComponentState).map(int[]::clone).toArray(int[][]::new);

        for (int i = 0; i < 2 * numberOfRepairsPerChromosome; i += 2) {
            int system = solution.getVariable(i);
            int component = solution.getVariable(i+1);
            copy[system][component] = 100;
            cost += perComponentCost[system][component];
            cost += initialComponentState[system][component];
        }

        for (int i = 0; i < numberOfSystems; i++) {
            boolean isAvailable = true;
            for (int j = 0; j < numberOfComponents; j++) {
                if (copy[solution.getVariable(i)][j] == 0) {
                    isAvailable = false;
                    break;
                }
            }
            if (isAvailable) {
                availableSystems++;
            }
        }
        double downSystems = numberOfSystems - availableSystems;
        solution.setObjective(0, cost * 1.0);
        solution.setObjective(1, downSystems * 1.0);
        return solution;
    }

    @Override
    public IntegerSolution createSolution() {
        return new DefaultIntegerSolution(getNumberOfObjectives(), getNumberOfConstraints(), integerBounds);
    }
}
