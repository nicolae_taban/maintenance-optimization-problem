package crossover;

import org.apache.commons.lang3.ArrayUtils;
import org.uma.jmetal.operator.crossover.CrossoverOperator;
import org.uma.jmetal.solution.integersolution.IntegerSolution;
import org.uma.jmetal.util.errorchecking.JMetalException;
import org.uma.jmetal.util.pseudorandom.JMetalRandom;

import java.util.ArrayList;
import java.util.List;

public class MaintenanceProblemCrossover implements CrossoverOperator<IntegerSolution> {
    private final JMetalRandom randomNumberGenerator = JMetalRandom.getInstance();
    private final double probability;
    private final int crossovers;
    private final int numberOfSystems;
    private final int numberOfComponents;
    private final int numberOfRepairs;

    public MaintenanceProblemCrossover(double probability, int crossovers, int numberOfSystems, int numberOfComponents, int numberOfRepairs) {
        if (probability < 0.0) throw new JMetalException("Probability can't be negative");
        if (crossovers < 1) throw new JMetalException("Number of crossovers is less than one");
        this.probability = probability;
        this.crossovers = crossovers;
        this.numberOfSystems = numberOfSystems;
        this.numberOfComponents = numberOfComponents;
        this.numberOfRepairs = numberOfRepairs;
    }


    @Override
    public double getCrossoverProbability() {
        return probability;
    }

    @Override
    public int getNumberOfRequiredParents() {
        return 2;
    }

    @Override
    public int getNumberOfGeneratedChildren() {
        return 2;
    }

    @Override
    public List<IntegerSolution> execute(List<IntegerSolution> solutions) {
        if (null == solutions) {
            throw new JMetalException("Null parameter") ;
        } else if (getNumberOfRequiredParents() != solutions.size()) {
            throw new JMetalException("Integer Point Crossover requires + " + getNumberOfRequiredParents() + " parents, but got " + solutions.size());
        } else if (randomNumberGenerator.nextDouble() < probability) {
            return doCrossover(solutions);
        } else {
            return solutions;
        }
    }

    private List<IntegerSolution> doCrossover(List<IntegerSolution> solutions) {
        IntegerSolution mom = (IntegerSolution)solutions.get(0).copy();
        IntegerSolution dad = (IntegerSolution)solutions.get(1).copy();

        if (mom.getNumberOfVariables() != dad.getNumberOfVariables()) {
            throw new JMetalException("The 2 parents don't have the same number of variables");
        }
        if (mom.getNumberOfVariables() < crossovers) {
            throw new JMetalException("The number of crossovers is higher than the number of variables");
        }

        int[] crossoverPoints = new int[crossovers];
        for (int i = 0; i < crossoverPoints.length; i++) {
            crossoverPoints[i] = randomNumberGenerator.nextInt(0, mom.getNumberOfVariables() - 1);
        }
        IntegerSolution girl = (IntegerSolution) mom.copy();
        IntegerSolution boy = (IntegerSolution) dad.copy();
        boolean swap = false;

        for (int i = 0; i < mom.getNumberOfVariables(); i++) {
            if (swap) {
                boy.setVariable(i, mom.getVariable(i));
                girl.setVariable(i, dad.getVariable(i));

            }

            if (ArrayUtils.contains(crossoverPoints, i)) {
                swap = !swap;
            }
        }
        List<IntegerSolution> result = new ArrayList<>();


        makeOffspringUnique(girl);
        result.add(girl);
        makeOffspringUnique(boy);
        result.add(boy);
        return result;
    }

    private void makeOffspringUnique(IntegerSolution girl) {
        List<ArrayList<Integer>> usedCombinations = new ArrayList<>();
        for (int i = 0; i < numberOfSystems; i++) {
            usedCombinations.add(i, new ArrayList<>());
        }

        for (int i = 0; i < numberOfRepairs * 2; i+=2) {
            ArrayList<Integer> theList = usedCombinations.get(girl.getVariable(i));
            if (!theList.contains(girl.getVariable(i+1))) {
                theList.add(girl.getVariable(i+1));
                usedCombinations.set(girl.getVariable(i), theList);
            } else {
                int randomSystem = randomNumberGenerator.nextInt(0, numberOfSystems - 1);
                int randomComponent = randomNumberGenerator.nextInt(0, numberOfComponents - 1);
                while (usedCombinations.get(randomSystem).contains(randomComponent)) {
                    randomSystem = randomNumberGenerator.nextInt(0, numberOfSystems - 1);
                    randomComponent = randomNumberGenerator.nextInt(0, numberOfComponents - 1);
                }
                usedCombinations.get(randomSystem).add(randomComponent);
                girl.setVariable(i, randomSystem);
                girl.setVariable(i+1, randomComponent);
            }
        }
    }
}
